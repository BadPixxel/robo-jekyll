<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Commands;

use BadPixxel\Robo\Jekyll\JekyllTasksTrait;
use BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;
use BadPixxel\Robo\NodeJs\Robo\Plugin\Tasks\Yarn;
use BadPixxel\Robo\NodeJs\YarnTasksTrait;
use Robo\Result;
use Robo\Symfony\ConsoleIO;

class JekyllCommands extends \Robo\Tasks
{
    use JekyllTasksTrait;
    use YarnTasksTrait;

    /**
     * @command jekyll:compile
     *
     * @description Just Compile Jekyll Documentation Sources
     *
     * @return int
     */
    public function compile(ConsoleIO $consoleIo): int
    {
        //====================================================================//
        // Compile Jekyll Website Sources
        /** @var Result $compile */
        $compile = $this->taskJekyllCompile()->run();
        if (!$compile->wasSuccessful()) {
            $consoleIo->error($compile->getMessage());

            return 1;
        }

        return 0;
    }

    /**
     * @command jekyll:build
     *
     * @description Build Jekyll Project Documentation
     *
     * @return int
     */
    public function build(ConsoleIO $consoleIo): int
    {
        //====================================================================//
        // Create Tmp Path (deleted when program exits)
        $tmpDir = $this->_tmpDir();
        //====================================================================//
        // Compile Jekyll Website Sources
        /** @var Jekyll\Compile $compile */
        $compile = $this->taskJekyllCompile();
        $compile
            ->tempDir($tmpDir)
        ;
        if (!$compile ->run()->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Install Yarn Packages
        /** @var Yarn\Install $yarn */
        $yarn = $this->taskYarnInstall();
        $yarn
            ->sources($tmpDir)
            ->target("/assets/vendor")
        ;
        if (!$yarn->run()->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Build Jekyll Website
        /** @var Jekyll\Build $build */
        $build = $this->taskJekyllBuild();
        $build->sources($tmpDir);
        if (!$build->run()->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Export Jekyll Website
        /** @var Jekyll\Export $export */
        $export = $this->taskJekyllExport();
        $export->tempDir($tmpDir);
        if (!$export->run()->wasSuccessful()) {
            return 1;
        }
        $consoleIo->success("Jekyll Build Done");

        return 0;
    }

    /**
     * @command jekyll:serve
     *
     * @description Serve/Preview Jekyll Project Documentation
     *
     * @param bool $watch
     *
     * @return int
     */
    public function serve(ConsoleIO $consoleIo, bool $watch = true): int
    {
        //====================================================================//
        // Create Tmp Path (deleted when program exits)
        $tmpDir = $this->_tmpDir();
        //====================================================================//
        // Compile Jekyll Website Sources
        /** @var Jekyll\Compile $compile */
        $compile = $this->taskJekyllCompile();
        $compile->tempDir($tmpDir);
        if (!$compile->run()->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Install Yarn Packages
        /** @var Yarn\Install $yarnInstall */
        $yarnInstall = $this->taskYarnInstall();
        $yarnInstall
            ->sources($tmpDir)
            ->target("/assets/vendor")
        ;
        if (!$yarnInstall->run()->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Serve Jekyll Website
        /** @var Jekyll\Serve $jekyllServe */
        $jekyllServe = $this->taskJekyllServe();
        $jekyllServe->sources($tmpDir);
        if (!$jekyllServe->run()->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Watch for Changes
        if (!$watch) {
            return 0;
        }
        /** @var Jekyll\Watch $watch */
        $watch = $this->taskJekyllWatch();
        $watch->tempDir($tmpDir)->run();

        $consoleIo->success("Jekyll Serve Done");

        return 0;
    }
}
