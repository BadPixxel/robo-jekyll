<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;

use Robo\Common\BuilderAwareTrait;

/**
 * Serve Jekyll Application via Docker
 */
class Serve extends AbstractDockerTask
{
    use BuilderAwareTrait;

    /**
     * Enable Incremental Builds
     *
     * @var bool
     */
    private bool $incremental = true;

    /**
     * Set Incremental Builds
     *
     * @param bool $incremental
     *
     * @return $this
     */
    public function incremental(bool $incremental = true): self
    {
        $this->incremental = $incremental;

        return $this;
    }

    /**
     * Get Command To Execute
     *
     * @return string
     */
    public function getCommand(): string
    {
        return sprintf(
            'jekyll serve %s',
            $this->incremental ? "--incremental --watch --force_polling" : ""
        );
    }

    /**
     * @inheritDoc
     */
    protected function isDetached(): bool
    {
        return true;
    }
}
