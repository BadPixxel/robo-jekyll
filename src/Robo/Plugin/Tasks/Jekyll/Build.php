<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;

use Robo\Common\BuilderAwareTrait;

/**
 * Build Jekyll Website via Docker
 */
class Build extends AbstractDockerTask
{
    use BuilderAwareTrait;

    /**
     * Get Command To Execute
     *
     * @return string
     */
    public function getCommand(): string
    {
        return 'jekyll build';
    }

    /**
     * @inheritDoc
     */
    protected function isDetached(): bool
    {
        return false;
    }
}
