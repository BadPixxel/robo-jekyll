<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;

use Robo\Result;

/**
 * Compile Jekyll Website Sources
 */
class Compile extends AbstractCompileTaskTask
{
    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        //====================================================================//
        // Build Disabled => Skip this Task
        if (!$this->isEnabled()) {
            return Result::error($this, "Jekyll Builder is Disabled");
        }
        //====================================================================//
        // Copy All Site Contents to Build Directory
        if (!$this->mirrorSiteContents()) {
            return Result::error($this, "Contents Copy Failed");
        }
        //====================================================================//
        // Build Site Configuration
        if (!$this->buildConfig()) {
            return Result::error($this, "Site Config Building Failed");
        }

        return Result::success($this, "Jekyll Builder is Done");
    }
}
