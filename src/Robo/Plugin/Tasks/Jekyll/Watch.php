<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;

use Lurker\Event\FilesystemEvent;
use Robo\Result;
use Robo\Task\Base\Watch as RoboWatch;

/**
 * Watch changes on Jekyll Website Sources
 */
class Watch extends AbstractCompileTaskTask
{
    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        //====================================================================//
        // Build Disabled => Skip this Task
        if (!$this->isEnabled()) {
            return Result::error($this, "Jekyll Builder is Disabled");
        }
        //====================================================================//
        // Enable Watch
        /** @var RoboWatch $watchStack */
        $watchStack = $this->taskWatch();
        foreach (array_keys($this->getSiteContentsPaths()) as $path) {
            if (!realpath($path)) {
                continue;
            }
            $watchStack->monitor(
                $path,
                function () {
                    $this->mirrorSiteContents(false);
                },
                FilesystemEvent::ALL
            );
        }
        $watchStack->run();

        return Result::success($this, "Jekyll Source Watch Enabled");
    }
}
