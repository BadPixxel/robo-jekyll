<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Robo\Task\BaseTask;
use Robo\Task\Docker\Remove;
use Robo\Task\Docker\Run;

/**
 * Serve Jekyll Application via Docker
 */
abstract class AbstractDockerTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * Path to Jekyll Sources
     *
     * @var string
     */
    protected string $sources;

    /**
     * Jekyll Docker Image to Use
     *
     * @var string
     */
    //    protected string $image = "jekyll/minimal";
    protected string $image = "jekyll/jekyll";

    /**
     * Jekyll Docker Container Name
     *
     * @var string
     */
    protected string $name = "badpixxel-jekyll";

    /**
     * Server Port
     *
     * @var int
     */
    protected int $port = 4000;

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        //====================================================================//
        // Safety Check
        if (empty($this->sources) || empty(realpath($this->sources))) {
            return Result::error($this, "No Jekyll Sources Path Defined");
        }
        //====================================================================//
        // Remove Any Existing Docker Container
        $this->taskDockerRemove($this->name)->run();
        //====================================================================//
        // Execute Jekyll Command via Docker
        /** @var Run $dockerRun */
        $dockerRun = $this->taskDockerRun($this->image);
        $result = $dockerRun
            ->name($this->name)
            ->interactive(false)
            ->background($this->isDetached())
            ->volume(realpath($this->sources), "/srv/jekyll:Z")
            ->volume($this->name."-bundles", "/usr/local/bundle:Z")
            ->publish(4000, $this->port)
            ->exec($this->getCommand())
            ->run()
        ;

        //====================================================================//
        // Return Action Result
        return ($result->wasSuccessFul() || $this->isDetached())
            ? Result::success($this, "Jekyll Command Done")
            : Result::error($this, "Jekyll Command Fail")
        ;
    }

    /**
     * Set Sources Path
     *
     * @param string $sources
     *
     * @return $this
     */
    public function sources(string $sources): self
    {
        $this->sources = $sources;

        return $this;
    }

    /**
     * Set Docker Image to Use
     *
     * @param string $image
     *
     * @return $this
     */
    public function image(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Set Port
     *
     * @param int $port
     *
     * @return $this
     */
    public function port(int $port): self
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Get Yarn Command to Execute
     *
     * @return string
     */
    abstract protected function getCommand(): string;

    /**
     * Detach Docker Container ?
     *
     * @return bool
     */
    abstract protected function isDetached(): bool;
}
