<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Task\BaseTask;
use Robo\Task\File\Write;
use Robo\Task\Filesystem\FilesystemStack;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Abstract Class to Compile Jekyll Website Sources
 */
abstract class AbstractCompileTaskTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * Path of Jekyll Base Site (Relative to Sdk)
     */
    const SRC_PATH = "src/Resources/jekyll";

    /**
     * Path of Final Docs (Relative to Project)
     */
    const TARGET_PATH = './public';

    /**
     * Path of Modules Documentations Contents (Relative to Sdk)
     */
    const LOCAL_PATH = 'src/Resources/docs';

    /**
     * Generic Contents Path (Relative to Project or Sdk)
     */
    const GENERIC_PATHS = array(
        "src/Resources/contents",
        "jekyll"
    );

    /**
     * Generic Contents To Add
     */
    const GENERIC_CONTENTS = array(
        "php-composer"
    );

    /**
     * Temp Folder for Building the Site
     *
     * @var string
     */
    private string $tmpPath = './build/jekyll';

    //====================================================================//
    // TASKS CONFIGURATION
    //====================================================================//

    /**
     * Set Temporary Build Path
     *
     * @param string $tmpPath
     *
     * @return $this
     */
    public function tempDir(string $tmpPath): self
    {
        $this->tmpPath = $tmpPath;

        return $this;
    }

    /**
     * Check if Documentation Building is Enabled
     *
     * @return bool
     */
    protected function isEnabled(): bool
    {
        return !empty($this->getConfigValue("enabled", true));
    }

    /**
     * Get Temp Build Directory Path
     *
     * @return string
     */
    protected function getTmpPath(): string
    {
        return $this->tmpPath;
    }

    /**
     * Get Documentations Target Directory Path
     *
     * @return string
     */
    protected function getTargetDirectory(): string
    {
        $path = $this->getConfigValue("target");

        return is_string($path) ? $path : self::TARGET_PATH;
    }

    /**
     * Get Jekyll Sources Directory Path
     *
     * @return string
     */
    protected function getJekyllSrcDirectory(): string
    {
        $path = $this->getConfigValue("jekyll_source");

        return (string) $this->resolvePath(
            is_string($path) ? $path : self::SRC_PATH
        );
    }

    /**
     * Get Generic Contents Directory Path
     *
     * @return string[]
     */
    protected function getGenericDirectories(): array
    {
        $genericPaths = $this->getConfigValue("generic_paths");

        return array_merge(is_array($genericPaths) ? $genericPaths : array(), self::GENERIC_PATHS);
    }

    /**
     * Get Generic Contents Directory Path
     *
     * @return string[]
     */
    protected function getGenericContentCodes(): array
    {
        $genericContents = $this->getConfigValue("generic_contents");

        return is_array($genericContents) ? $genericContents : self::GENERIC_CONTENTS;
    }

    /**
     * Get Local Sources Directory Path
     *
     * @return string
     */
    protected function getLocalContentsDirectory(): string
    {
        $path = $this->getConfigValue("source");

        return is_string($path) ? $path : self::LOCAL_PATH;
    }

    /**
     * Get Splash Manifest Path
     *
     * @return string
     */
    protected function getManifestPath(): string
    {
        return "./splash.yml";
    }

    //====================================================================//
    // TASKS EXECUTION
    //====================================================================//

    /**
     * Build Site Directory & Add Contents
     *
     * @param bool $clean
     *
     * @return bool
     */
    protected function mirrorSiteContents(bool $clean = true): bool
    {
        /** @var FilesystemStack $fsStack */
        $fsStack = $this->taskFilesystemStack();
        //====================================================================//
        // Init Module Build Directory
        if ($clean) {
            if (is_dir($this->getTmpPath())) {
                $this->taskCleanDir($this->getTmpPath())->run();
            }
            $fsStack->mkdir($this->getTmpPath());
        }
        //====================================================================//
        // All Site Contents Contents
        foreach ($this->getSiteContentsPaths() as $source => $target) {
            $fsStack->mirror(
                $source,
                $target,
                null,
                array("override" => true)
            );
        }

        //====================================================================//
        // Execute Filesystem Actions
        return $fsStack->run()->wasSuccessFul();
    }

    /**
     * Get List of Directories Used for Building Site Sources
     *
     * @return array<string, string>
     */
    protected function getSiteContentsPaths(): array
    {
        $files = array(
            $this->getJekyllSrcDirectory() => $this->getTmpPath()
        );
        //====================================================================//
        // Copy Generic Contents
        foreach ($this->getGenericContentCodes() as $code) {
            foreach ($this->getGenericContentDirectories($code) as $contentDir) {
                $files[$contentDir] = $this->getTmpPath();
            }
        }
        //====================================================================//
        // Copy Local Contents
        $files[$this->getLocalContentsDirectory()] = $this->getTmpPath();

        //====================================================================//
        // Execute Filesystem Actions
        return $files;
    }

    /**
     * Build Site Configuration
     *
     * @return bool
     */
    protected function buildConfig(): bool
    {
        try {
            //====================================================================//
            // Load Generic Configuration
            /** @var array $coreConfig */
            $coreConfig = Yaml::parseFile($this->getJekyllSrcDirectory().'/_config.yml') ?? array();
            //====================================================================//
            // Load Local Configuration
            /** @var array $localConfig */
            $localConfig = Yaml::parseFile($this->getLocalContentsDirectory().'/_config.yml') ?? array();
            //====================================================================//
            // Load Module Splash Manifest
            $manifest = array("manifest" => null);
            if (is_file($this->getManifestPath())) {
                $manifest = array("manifest" => Yaml::parseFile($this->getManifestPath()));
            }
        } catch (ParseException $exception) {
            $this->printTaskError("Unable to load Jekyll Configuration: ".$exception->getMessage());

            return false;
        }
        //====================================================================//
        // Build Final Configuration
        $finalConfig = array_replace_recursive($coreConfig, $localConfig, $manifest);
        //====================================================================//
        // Write Final Configuration
        /** @var Write $writeToFile */
        $writeToFile = $this->taskWriteToFile($this->getTmpPath().'/_config.yml');
        $writeToFile->text(Yaml::dump($finalConfig, 5));

        return $writeToFile->run()->wasSuccessFul();
    }

    /**
     * Resolve Relative Path depending on Run Context
     *
     * @param string $basePath Relative path
     *
     * @return null|string Absolute File Path
     */
    protected function resolvePath(string $basePath): ?string
    {
        //====================================================================//
        // Build List of Possible Paths
        $pathResolvers = array(
            // Relative to Current Location
            './%s',
            // Relative to This Library Root Path
            dirname(__DIR__, 5).'/%s',
        );
        $pharPath = \Phar::running(true);
        if ($pharPath) {
            $pathResolvers[] = $pharPath."/%s";
            $pathResolvers[] = $pharPath."/vendor/badpixxel/robo-jekyll/%s";
        }
        //====================================================================//
        // Walk on Possible Content Paths
        foreach ($pathResolvers as $pathResolver) {
            $realPath = sprintf($pathResolver, $basePath);
            if (!is_dir($realPath)) {
                continue;
            }
            $this->printTaskInfo(sprintf("<comment>Resolved</comment> %s =>> %s", $basePath, $realPath));

            return $realPath;
        }
        $this->printTaskInfo(sprintf("<comment>Unresolved</comment> %s", $basePath));

        return null;
    }

    //====================================================================//
    // CORE METHODS
    //====================================================================//

    /**
     * Setup Task Config Namespace
     *
     * @param string $classname
     *
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected static function configClassIdentifier($classname): string
    {
        return "jekyll";
    }

    /**
     * Get Generic Contents Directory Path
     *
     * @param string $contentsDir
     *
     * @return string[]
     */
    private function getGenericContentDirectories(string $contentsDir): array
    {
        $paths = array();
        //====================================================================//
        // Walk on Generic Content Paths
        foreach ($this->getGenericDirectories() as $genericPath) {
            $paths[] = (string) $this->resolvePath($genericPath."/".$contentsDir);
        }

        return array_unique(array_filter($paths));
    }
}
