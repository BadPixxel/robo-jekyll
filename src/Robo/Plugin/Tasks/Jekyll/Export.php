<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;

use Robo\Result;
use Robo\Task\Filesystem\FilesystemStack;

/**
 * Extort Jekyll Website Build to Target Directory
 */
class Export extends AbstractCompileTaskTask
{
    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        //====================================================================//
        // Build Disabled => Skip this Task
        if (!$this->isEnabled()) {
            return Result::error($this, "Jekyll Builder is Disabled");
        }
        //====================================================================//
        // Clear Build Directory
        if (is_dir($this->getTargetDirectory())) {
            $this->taskCleanDir($this->getTargetDirectory())->run();
        }
        //====================================================================//
        // Copy Jekyll Build Contents
        /** @var FilesystemStack $fsStack */
        $fsStack = $this->taskFilesystemStack();
        $fsStack
            ->mkdir($this->getTargetDirectory())
            ->mirror($this->getTmpPath()."/_site", $this->getTargetDirectory())
        ;
        //====================================================================//
        // Build Site Configuration
        if (!$fsStack->run()->wasSuccessFul()) {
            return Result::error($this, "Jekyll Site Export Failed");
        }

        return Result::success($this, "Jekyll Site Export is Done");
    }
}
