---
lang: en
permalink: start/console
title: Robo Console
---

This package add Jekyll actions as [Robo](https://Robo.li/) tasks.

### List available commands

Once installed, Jekyll commands are available on Robo console.
You can see list of available commands using.

```bash
$  php vendor/bin/robo
```

```bash
  Robo 4.0.3

  Usage:
    command [options] [arguments]

  Available commands:

  jekyll
    jekyll:build    Build Jekyll Project Documentation
    jekyll:compile  Just Compile Jekyll Documentation Sources
    jekyll:serve    Serve/Preview Jekyll Project Documentation
```

&nbsp;
### Build Command

This command is done for building you whole Documentation static website.

```bash
$  php vendor/bin/robo jekyll:build
```

By default, sources are taken from **src/Resources/docs** path and pushed in **./public/** once build is done.

Build has many technical steps:
* All Sources are copied to a temporary path.
* Assets are loaded using Yarn inside a Docker container.
* Jekyll website build is executed a Docker container.

&nbsp;
### Serve Command

This command will build you website in a temporary path, then serve it inside a Docker container.

```bash
$  php vendor/bin/robo jekyll:serve
```

Once build is done, you can access your website at **http://0.0.0.0/my-project/**

&nbsp;
### Compile Command (Debug)

Compile command will only copy whole sources to a local temporary path (Default is **./build/jekyll**).

```bash
$  php vendor/bin/robo jekyll:compile
```

This way you can see original sources that will be used for building your website.
 
&nbsp;
### Advanced Configuration

Most part of website contents may be customized. Website options are stored in **robo.yml** file that MUST reside on root path of your project.

<div class="warning">
	See advanced configuration to change default settings
</div>
