<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Jekyll;

use BadPixxel\Robo\Jekyll\Robo\Plugin\Tasks\Jekyll;
use Robo\Collection\CollectionBuilder;

/**
 * Import jekyll Tasks for Robo
 */
trait JekyllTasksTrait
{
    /**
     * Get Jekyll Compile Task
     *
     * @return CollectionBuilder|Jekyll\Compile
     */
    protected function taskJekyllCompile()
    {
        return $this->task(Jekyll\Compile::class);
    }

    /**
     * Get Jekyll Watch Task
     *
     * @return CollectionBuilder|Jekyll\Watch
     */
    protected function taskJekyllWatch()
    {
        return $this->task(Jekyll\Watch::class);
    }

    /**
     * Get Jekyll Build Task
     *
     * @return CollectionBuilder|Jekyll\Build
     */
    protected function taskJekyllBuild()
    {
        return $this->task(Jekyll\Build::class);
    }

    /**
     * Get Jekyll Build Task
     *
     * @return CollectionBuilder|Jekyll\Export
     */
    protected function taskJekyllExport()
    {
        return $this->task(Jekyll\Export::class);
    }

    /**
     * Get Jekyll Serve Task
     *
     * @return CollectionBuilder|Jekyll\Serve
     */
    protected function taskJekyllServe()
    {
        return $this->task(Jekyll\Serve::class);
    }
}
