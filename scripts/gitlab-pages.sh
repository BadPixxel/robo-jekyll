#!/bin/sh
################################################################################
#
# Copyright (C) 2020 BadPixxel <www.badpixxel.com>
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#
################################################################################

################################################################
# Force Failure if ONE line Fails
set -e

echo "----------------------------------------------------"
echo "--> Install PHP & Extensions"
echo "----------------------------------------------------"
apk update && apk upgrade
apk add php  php-curl php-json php-phar php-iconv php-openssl

echo "----------------------------------------------------"
echo "--> Install Wall-E from Github"
echo "----------------------------------------------------"
wget https://github.com/BadPixxel/Php-Robo/blob/main/bin/wall-e.phar?raw=true -O /usr/local/bin/wall-e
chmod +x /usr/local/bin/wall-e
wall-e

echo "----------------------------------------------------"
echo "--> Compile Website Sources"
echo "----------------------------------------------------"
wall-e jekyll:compile

echo "----------------------------------------------------"
echo "--> Compile Assets"
echo "----------------------------------------------------"
cd build/jekyll
chmod 777 -Rf ./
yarn install --modules-folder=./assets/vendor

echo "----------------------------------------------------"
echo "--> Build Website"
echo "----------------------------------------------------"
mkdir -p .jekyll-cache _site
jekyll build
mkdir -p ./../../public
cp -Rf ./_site/* ./../../public/
